package daniel.testing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Locale;

import com.ibm.icu.text.NumberFormat;

@SuppressWarnings("serial")
public class Currency implements Comparable<Currency>, Serializable {

	private BigDecimal value;
	
	public Currency(int value) {
		this.value = new BigDecimal(value);
	}
	
	public Currency(double value) {
		this.value = BigDecimal.valueOf(value);
	}
	
	public Currency(String value) {
		this.value = new BigDecimal(value);
	}
	
	public BigDecimal getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		NumberFormat n = NumberFormat.getCurrencyInstance(Locale.CANADA);
		return n.format(value).substring(1);
	}

	@Override
	public int compareTo(Currency o) {
		// TODO Auto-generated method stub
		return value.compareTo(o.getValue());
	}
}
