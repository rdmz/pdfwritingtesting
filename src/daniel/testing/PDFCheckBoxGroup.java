package daniel.testing;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckbox;

public class PDFCheckBoxGroup {
	private PDDocument pdfDocument;
	private ArrayList<String> checkboxNames;
	
	public PDFCheckBoxGroup(PDDocument pdfDocument, ArrayList<String> checkboxNames) {
		this.pdfDocument = pdfDocument;
		this.checkboxNames = checkboxNames;
	}
	
	public void checkField(String name) throws IOException {
		int length = checkboxNames.size();
		for (int i = 0; i < length; ++i) {
			String currentCheckBoxName = checkboxNames.get(i);
			PDCheckbox checkBox = getCheckBox(currentCheckBoxName);
			if (currentCheckBoxName.equals(name)) {
				checkBox.check();
			} else {
				checkBox.unCheck();
			}
		}
	}
	
	private PDCheckbox getCheckBox(String name) throws IOException {
		PDDocumentCatalog catalog = pdfDocument.getDocumentCatalog();
		PDAcroForm acroForm = catalog.getAcroForm();
		PDCheckbox field = (PDCheckbox)acroForm.getField(name);
		return field;
	}
	
	public ArrayList<String> getCheckBoxNames() {
		return checkboxNames;
	}
	
	public void setCheckBoxNames(ArrayList<String> checkBoxNames) {
		this.checkboxNames = checkBoxNames;
	}
}
