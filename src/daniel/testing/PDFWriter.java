package daniel.testing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

public abstract class PDFWriter {
	private String srcPath;
	private PDDocument pdfDocument;
	
	public PDFWriter(String src) throws IOException{
		this.srcPath = src;
		this.pdfDocument = PDDocument.load(src);
	}
	
	protected void setField(String name, String value) throws IOException {
		PDDocumentCatalog dataCatalog = pdfDocument.getDocumentCatalog();
		PDAcroForm acroForm = dataCatalog.getAcroForm();
		PDField field = acroForm.getField(name);
		if (field != null)
			field.setValue(value);
		else
			System.err.println("No field found with that name: " + name);
	}
	
	protected PDField getField(String name) throws IOException {
		PDDocumentCatalog dataCatalog = pdfDocument.getDocumentCatalog();
		PDAcroForm acroForm = dataCatalog.getAcroForm();
		return acroForm.getField(name);
	}
	
	protected void closePDF() throws IOException {
		this.pdfDocument.close();
	}
	
	public String getSrc() {
		return this.srcPath;
	}
	
	protected PDFCheckBoxGroup createCheckBoxGroup(ArrayList<String> list) {
		return new PDFCheckBoxGroup(pdfDocument, list);
	}
	
	@SuppressWarnings("unchecked")
	protected List<PDPage> getPDFPages() {
		PDDocumentCatalog dataCatalog = pdfDocument.getDocumentCatalog();
		return dataCatalog.getAllPages();
	}
	
	public abstract void createPdf() throws IOException;
}
