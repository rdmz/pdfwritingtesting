package daniel.testing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

public class PDFWriterHelper {

	private List<PDFWriter> pdfWriters;
	
	public PDFWriterHelper() {
		this.pdfWriters = new ArrayList<PDFWriter>();
	}
	
	private void mergePdfPages(List<PDPage> pages, PDDocument document) {
		for (PDPage page : pages) {
			document.addPage(page);
		}
	}
	
	public void mergePdfWriters(String dest) throws COSVisitorException, IOException {
		PDDocument document = new PDDocument();
		for (PDFWriter writer : pdfWriters) {
			mergePdfPages(writer.getPDFPages(), document);
			writer.createPdf();
		}
		document.save(dest);
		document.close();
	}
	
	public void addPDFWriter(PDFWriter writer) {
		pdfWriters.add(writer);
	}
	
	public boolean removePDFWriter(PDFWriter writer) {
		return pdfWriters.remove(writer);
	}
	
	public PDFWriter createSchedule6PDFWriter(String dest) throws IOException {
		PDFWriter writer = new Schedule6PDFWriter(new Schedule6ModelTest(), dest);
		pdfWriters.add(writer);
		return writer;
	}
	
	public void closePdfWriters() throws IOException {
		for (PDFWriter writer : pdfWriters)
			writer.closePDF();
	}
}
