package daniel.testing;

import java.math.BigDecimal;

public class Schedule6ModelTest {
	private String line_381;
	private String line_382;
	private Currency col1line3;
	
	public Schedule6ModelTest() {
		
	}
	
	public String getLine381() {
		return line_381;
	}
	
	public void setLine381(String line_381) {
		this.line_381 = line_381;
	}
	
	public String getLine382() {
		return line_382;
	}
	
	public void setLine382(String line_382) {
		this.line_382 = line_382;
	}
	
	public void setColumn1Line3(Currency number) {
		this.col1line3 = number;
	}
	
	public Currency getColumn1Line3() {
		return this.col1line3;
	}
}
