package daniel.testing;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

public class Schedule6PDFWriter extends PDFWriter{

	private Schedule6ModelTest schedule6;
	
	private PDFCheckBoxGroup eligibleDependant381;
	private PDFCheckBoxGroup eligibleSpouse382;
	
	public Schedule6PDFWriter(Schedule6ModelTest schedule6, String src) throws IOException {
		super(src);
		this.schedule6 = schedule6;
		this.schedule6.setLine381("N");
		this.schedule6.setLine382("Y");
		this.schedule6.setColumn1Line3(new Currency("349872394.00"));
		init381DependantCheckBoxGroup();
		init382SpouseCheckBoxGroup();
	}
	
	private void init381DependantCheckBoxGroup() {
		ArrayList<String> dependantFieldNames = new ArrayList<String>();
		dependantFieldNames.add("form1[0].Page1[0].Step1_Sub[0].Line381_Sub[0].Line381_CheckBoxGroup[0].Line381_CheckBox_EN[0]");
		dependantFieldNames.add("form1[0].Page1[0].Step1_Sub[0].Line381_Sub[0].Line381_CheckBoxGroup[0].Line381_CheckBox_EN[1]");
		this.eligibleDependant381 = createCheckBoxGroup(dependantFieldNames);
	}
	
	private void init382SpouseCheckBoxGroup() {
		ArrayList<String> spouseFieldNames = new ArrayList<String>();
		spouseFieldNames.add("form1[0].Page1[0].Step1_Sub[0].Line382_Sub[0].Line382_CheckBoxGroup[0].Line382_CheckBox_EN[0]");
		spouseFieldNames.add("form1[0].Page1[0].Step1_Sub[0].Line382_Sub[0].Line382_CheckBoxGroup[0].Line382_CheckBox_EN[1]");
		this.eligibleSpouse382 = createCheckBoxGroup(spouseFieldNames);
	}
	
	@Override
	public void createPdf() throws IOException {
		checkLine381();
		checkLine382();
		setLine3Column1();
	}
	
	private void checkLine381() throws IOException {
		String line_381 = schedule6.getLine381();
		if (line_381.equals("N"))
			check381DependantNo();
		else if (line_381.equals("Y"))
			check381DependantYes();
		else
			System.err.println("Can only be a Y or N for line 381");
	}
	
	private void check381DependantNo() throws IOException {
		this.eligibleDependant381.checkField(this.eligibleDependant381.getCheckBoxNames().get(1));
	}
	
	private void check381DependantYes() throws IOException {
		this.eligibleDependant381.checkField(this.eligibleDependant381.getCheckBoxNames().get(0));
	}
	
	private void checkLine382() throws IOException {
		String line_382 = schedule6.getLine382();
		if (line_382.equals("N"))
			check382SpouseNo();
		else if (line_382.equals("Y"))
			check382SpouseYes();
		else
			System.err.println("Can only be a Y or N for line 382");
	}
	
	private void check382SpouseNo() throws IOException {
		this.eligibleSpouse382.checkField(this.eligibleSpouse382.getCheckBoxNames().get(1));
	}
	
	private void check382SpouseYes() throws IOException {
		this.eligibleSpouse382.checkField(this.eligibleSpouse382.getCheckBoxNames().get(0));
	}
	
	private void setLine3Column1() throws IOException {
		PDField field = getField("form1[0].Page1[0].Step1_Sub[0].PartA_sub[0].Line3_Sub[0].Column1_Line3_Box[0].Number_Field[0]");
		field.setValue(schedule6.getColumn1Line3().toString());
	}
}
